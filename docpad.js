module.exports = {

  prompts: false,

  // These are variables will be accessible via our templates
  templateData: {

    // Conference info
    conf: {
      name: "Conferência Missionária Steiger 2016",
      description: "Conferência Steiger 2016",
      date: "26 de novembro de 2016",
      title: "Conferência Steiger 2016 | Missão na Cidade",
      // If your event is free, just comment this line
      //price: "R$100",
      venue: "Catedral Metodista de São Paulo",
      address: "Avenida Liberdade, 659 - Liberdade",
      venueInfo: "A Catedral Metodista está próxima da Estação Liberdade do Metrô. Não há estacionamento disponível na igreja.",
      city: "São Paulo",
      state: "SP"
    },

    // The Call To Action button at the header,
    // If you don't want this, just remove the callToAction property.
    callToAction: {
        text: "Inscreva-se agora!",
        link: "#registration"
    },


    // Site info
    site: {
      theme: "yellow-swan",
      url: "http://conferencia.steiger.org/",
      registrationFormUrl: "https://goo.gl/forms/0GJG9BoJzP1Lh7wB3",
      contactFormUrl: "https://goo.gl/forms/HeI6SwO84OopwLJR2",
      googleanalytics: "UA-82393006-1"
    },

    // Active sections on the website
    // to deactivate comment out with '//'
    // you can also change order here and it will reflect on page
    sections: [
      'about',
      'location',
      'speakers',
      'lightning_talkers',
      'concerts',
      'schedule',
      // 'exhibitors',
      'registration',
      //'sponsors',
      //'partners'
      'contact'
    ],

    // Labels which you can translate to other languages
    labels: {
      about: "Sobre",
      location: "Localização",
      speakers: "Preletores",
      lightning_talkers: "M15",
      concerts: "Louvor",
      schedule: "Programação",
      exhibitors: "Expositores",
      registration: "Inscrições",
     // sponsors: "Apoio",
     // partners: "Partners",
      contact: "Contato"
    },

    // The entire schedule
    schedule: [
      {
        name: "Abertura – Oração/Orientação Geral",
        time: "08h30"
      },
	  {
        name: "Louvor",
        time: "08h40"
      },
	  {
        name: "Preleção 1",
        time: "9h00"
      },
	  {
        name: "Preleção 2",
        time: "9h45"
      },
	  {
        name: "✱Coffee Break Musical no Salão Social",
        time: "10h25"
      },
	  {
        name: "Preleção 3",
        time: "10h55"
      },
	  {
        name: "Preleção 4",
        time: "11h45"
      },
	  {
        name: "Preparação para Tarde",
        time: "12h30"
      },
	  {
        name: "INTERVALO",
        time: "12h30"
      },
	  {
        name: "M15",
        time: "14h00"
      },
	  {
        name: "Intervalo para recolhimento de perguntas",
        time: "15h00"
      },
	  {
        name: "Mesa de debates e respostas",
        time: "15h45"
      },
	  {
        name: "Louvor",
        time: "16h00"
      },
	  {
        name: "✱Coquetel no Salão Social",
        time: "17h00"
      },
	  {
        name: "Retorno",
        time: "18h00"
      },
	  {
        name: "Louvor",
        time: "18h10"
      },
	  {
        name: "Preleção 5",
        time: "19h30"
      },
	  {
        name: "Encerramento",
        time: "20h30"
      }
    ],
    
    speakers: [
        {
            name: "David Pierce",
            photo: "themes/yellow-swan/img/speakers/david-pierce-300x300.jpg",
            bio: "Fundador e ex-diretor executivo da Steiger International, evangelista, artista visionário e filósofo. O norte-americano David Pierce começou nos anos 1980 com estudos bíblicos para punks, anarquistas e jovens marginalizados em Amsterdã (HOL). A continuidade do trabalho permitiu surgir o projeto evangelístico No Longer Music, do qual é líder e até hoje vocalista. A banda já alcançou milhares de pessoas com sua proclamação em shows na América do Norte, América do Sul, Eurásia, Europa e Oceania. Uma audiência não alcançada e que nunca colocaria os pés na igreja: a Cultura Jovem Global, composta de pessoas tanto em lugares sombrios como confortáveis da sociedade, bem como conectada e distraída em comum. Autor dos livros “Revolutionary!” (BRA, 2015), “Rock Priest” (BRA, 2012), “Ratcage” (Steiger Press, 2004) e “Dancing with skinheads” (1998). Casado com Jodi Pierce, tem dois filhos e quatro netos.",
            company: "Steiger International",
            presentation: {},
        },
        {
            name: "Jodi Pierce",
            photo: "themes/yellow-swan/img/speakers/jodi-pierce.jpg",
            bio: "Jodi é norte-americana e atuou desde antes dos primeiros dias da banda No Longer Music na Holanda. Durante essa época, nos anos 1980, ela trazia estudos bíblicos para punks, góticos e militantes em um píer dos canais de Amsterdã, um local conhecido como “Steiger 14”. Jodi Pierce é fundadora da Steiger International. Iniciou a Steiger Missions School, então na Nova Zelândia, e depois foi Diretora da SMS na Alemanha, onde também atou como mentora e professora. Em sua jornada pelo alcance da Cultura Jovem Global, ela escreveu em 2007 o livro “Something Broken, Something extraordinary”. Casada com David Pierce, Jodi tem dois filhos e quatro netos.",
            company: "Steiger International",
            presentation: {},
        },
        {
            name: "Lukas Rueegger",
            photo: "themes/yellow-swan/img/speakers/lukas-rueegger.jpg",
            bio: "Lukas Rueegger é suíço, de Winterthur. Missionário Steiger atuante no Oriente Médio, que cria arte cinematográfica com alcance missionário através de filmes e debates. Um exemplo é RAWA, série de curta metragens que sonda criativa e artisticamente os ensinos de Jesus. Também conduz o evento \"criativo-líquido-evangelístico\" Sprouting, que une diferentes pessoas para re-explorar a espiritualidade cristã conforme elas trazem suas experiências e conhecimentos. Lukas tem trazido à Steiger a realidade de uma cultura jovem global secularizada além do ocidente. Ele estudou na Wirtschaftsschule KV Winterthur e na All Nations Christian College. Marido de Denise, eles têm dois filhos.",
            presentation: {},
        },
        {
            name: "Luke Grenwood",
            photo: "themes/yellow-swan/img/speakers/luke-greenwood.jpg",
            bio: "O missionário Steiger Luke Greenwood é inglês, mas viveu durante muito tempo no Brasil. Hoje é Diretor da Steiger Europa, líder na Steiger Polônia, e vocalista em turnês da banda No Longer Music. Ele atuou na Steiger Brasil por anos em sua implantação e fase pioneira, criando a banda Steiger NLM Alegórica (com a qual fez turnês evangelísticas pelo Brasil, Europa e no Líbano), e iniciando o Manifeste, a Conferência Steiger e a Escola Steiger Compacta. Foi Diretor da SMS (Steiger Missions School), em Krögis, em Sachsen (ALE). Conferencista e professor em treinamentos Steiger, tem formação em Missiologia pela All Nations Christians College, e graduação em Musicoterapia pela FAP Curitiba. Casado com Ania, é pai de dois filhos.",
            company: "Steiger Europa",
            presentation: {},
        },
        {
            name: "Sandro Baggio",
            photo: "themes/yellow-swan/img/speakers/sandro-baggio-400x400.png",
            bio: "Sandro Baggio é pastor-mentor do Projeto 242, missionário da Steiger International, sendo assim líder da Steiger Brasil e líder da Steiger Sudamerica. Formado em teologia com ênfase em missiologia pelo SEMITE-RJ, serviu como missionário junto a Operação Mobilização na Europa e a bordo do navio Logos II. Em 2003, fez o treinamento intensivo de líderes do Haggai em Maui, onde conheceu David Wong, de quem recebeu o livro do qual extraiu a frase “trilhando o estreito caminho entre o cinismo e a ingenuidade”. Seus dois livros publicados são “Música Cristã Contemporânea” (Editora Vida, 2005) e “Intelectualidade Cristã em Crise” (Editora Fôlego, 2007).",
            company: "Projeto 242",
            presentation: {},
        },
        
    ],
    
    lightning_talkers: [
        {
            name: "Angelo S. F. Davi",
            photo: "themes/yellow-swan/img/speakers/angelo.jpg",
            bio: "Comunicador social, cineasta, missionário Steiger e líder na Steiger Brasil. Foi diretor de arte publicitário e editorial. Realizou curtas e longa-metragens ficcionais e documentais como diretor-roteirista, e atua como editor-finalizador em produtoras e TV. Na Steiger Brasil foi líder do Manifeste, atua na Escola Compacta, na Comunicação e Captação, e no Evangelismo.",
            presentation: {},
        },
        {
            name: "Bruno Colisse",
            photo: "themes/yellow-swan/img/speakers/bruno.jpg",
            bio: "Vocalista, guitarrista e líder da nova formação da banda Steiger Alegórica. Integrante como baixista desde a primeira formação da banda, participou de turnês missionárias por diversos países. Também é programador e analista desenvolvedor.",
            presentation: {},
        },
        {
            name: "Café",
            photo: "themes/yellow-swan/img/speakers/cleverson.jpg",
            bio: "ou Cleverson Paes Pacheco, é artista e missionário. Tem arte nas ruas em Graffiti, mas que ao adentrar galerias viram obras de arte contemporânea. Faz exposições coletivas, ilustra espaços internos e externos, dentro e fora do Brasil. É arte educador em projetos sociais. Tem Licenciatura em Desenho pela EMBAP, e estuda Teologia no Seminário Teológico Betânia.",
            presentation: {},
        },
        {
            name: "Veronica Nobili",
            photo: "themes/yellow-swan/img/speakers/veronica.jpg",
            bio: "Formada em arte dramática, é Atriz e Dubladora, integrante da Cia. Do Quintal, e do Projeto BAC. Foi integrante da banda Alegórica como atriz no desempenho dramático dos shows, e assim participou de missões dentro e fora do Brasil.",
            presentation: {},
        },
    ],
    
    concerts: [
        {
            name: "Rodolfo Abrantes e O Muro de Pedra",
            photo: "themes/yellow-swan/img/speakers/rodolfo-abrantes.png",
            bio: "Rodolfo é cantor, compositor e guitarrista em diversos álbuns. Ele recentemente se juntou a Victor Pradella (Vocal e Guitarra), Thiago Tonini (Baixo) e Anderson Kuehne (Bateria) da banda O Muro de Pedra, para em conjunto fazer músicas, sem as amarras do mercado e rompendo barreiras de templos.",
            presentation: {},
        },
        {
            name: "Rede Ativa",
            photo: "themes/yellow-swan/img/speakers/rede-ativa.png",
            bio: "Banda de rock cristão que traz a o estilo combinado entre evangelístico e congregacional. Surgiu em meados de 2004, no culto de jovens da IEQ Ipiranga. Tem 4 discos lançados. Os componentes são Daniel Passamani (vocal e guitarra), Diego Montechessi (back e bateria), Anderson Gonçalves (guitarra) e Joao Filipe (baixo).",
            company: "Steiger International",
            presentation: {},
        },
    ],

    // List of Sponsors
    sponsors: [
      {
        name: "Eventick",
        logo: "themes/yellow-swan/img/sponsor.png",
        url: "http://eventick.com.br"
      }
    ],

    // List of Partners
    partners: [
      {
        name: "BrazilJS",
        logo: "themes/yellow-swan/img/partner.png",
        url: "http://braziljs.org"
      }
    ],

    // Theme path
    getTheme: function() {
      return "themes/" + this.site.theme;
    },

    // Site Path
    getUrl: function() {
    	return this.site.url;
    }
  }
};